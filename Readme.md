# About
The application fetches Web ARChrchives (WARCs) collected by Common Crawl and parses information about the used Javascript libraries from the pages stored within. The time required for important steps is measured and stored in a log file, with the goal to create comparable timing statistics.

## Application Flow
* prepare logging
* define a fetcher for the specified fetcher type (HTTP, S3)
* define a connector for the specified database type (RDS, Mongo)
* execute preflight in preparation for the parsing task
  * create databases
  * find crawls and WARCs within them, and prepare them for processing
* execute LibraryTask, responsible for the actual parsing
  * find unprocessed WARC
  * explore content of WARC to find stored pages
    * extract all JavaScript libraries of each page and cashe them locally
    * store url of each page in the database
  * after the entire WARC has been worked through, save sources in the database

## AWS Architecture
![architecture overview](docs/architecture.png)

# Local setup

## Prerequisites

* install Docker (https://docker.com)
* create `.env` file (refer to example_env for required variables)
* create the folders defined for `DATA_HOST_PATH` and `LOG_FOLDER`
* Initialize host as Docker swarm management node: `docker swarm init`
* load `.env` into environment variables (bash: `source .env`)
* create docker overlay network: `docker network create -d overlay --attachable $DATABASE_NETWORK`

## Start database

* execute `docker-compose -p $PROJECT_NAME -f database.docker-compose.yml up`

## Build parser
* `./build.sh parser`

## Start parser

* execute `docker-compose -p $PROJECT_NAME -f parser.docker-compose.yml up --scale parser=<count>` (replace count with count of parallel parsers to start)

## Build aggregator
* refer to "Build parser" and replace `parser` with `aggregator`

## Start aggregator

* execute `docker-compose -p $PROJECT_NAME -f aggregator.docker-compose.yml up`

## Additional hints

* if on Windows either use Cygwin (or WSL) or adapt the scripts (especially when calling environment variables)
* run `docker-compose up` with `-d` to start in background
* to then stop the containers run `docker-compse down` with the respective `-p` and `-f` parameters
* Docker may hint that orphan containers are found, when starting a component on the same host as others. This is expected (as the project name is the same, but the components are not in the same Docker compose file)
