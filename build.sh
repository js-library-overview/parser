#!/bin/bash
source .env
APP=$1
IMAGE="${APP^^}_IMAGE"
docker build -t ${!IMAGE} --target ${APP,,} .
