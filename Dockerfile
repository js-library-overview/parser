FROM python:3.7.5-alpine AS build
WORKDIR /usr/src/app
RUN apk add --update --no-cache g++ gcc libxslt-dev postgresql-dev python3-dev musl-dev
RUN pip install pipenv
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" VIRTUAL_ENV="/opt/venv"
COPY Pipfile Pipfile.lock /usr/src/app/
RUN pipenv install --deploy --dev

FROM python:3.7.5-alpine AS app
RUN apk add --update --no-cache libxslt-dev postgresql-dev
WORKDIR /usr/src/app
COPY --from=build /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" VIRTUAL_ENV="/opt/venv"
COPY . /usr/src/app

FROM app AS aggregator
CMD [ "python", "./src/aggregator.py" ]

FROM app AS parser
CMD [ "python", "./src/parser.py" ]
