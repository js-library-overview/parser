import unittest
from unittest.mock import MagicMock, patch

from __test__.__mocks__.static import static_crawl_paths as mock_static_crawl_paths
import src.database.preflight.Preflight as Preflight


class PreflightTestCase(unittest.TestCase):
    def setUp(self):
        self.preflight = Preflight.Preflight()

    def test_run(self):
        self.assertRaises(NotImplementedError, self.preflight.run, None, None)

    @patch('src.database.preflight.Preflight.static_crawl_paths')
    def test_fetch_crawl_paths(self, static_crawl_paths):
        static_crawl_paths.return_value = mock_static_crawl_paths
        fetcher: MagicMock = MagicMock()
        fetcher.get_coll_info = MagicMock(return_value=[None, None])

        response = self.preflight.fetch_crawl_paths(fetcher)

        assert not fetcher.get_coll_info.called
        assert response == mock_static_crawl_paths, f'response is {response}'
