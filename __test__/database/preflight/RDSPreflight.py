import unittest
from unittest.mock import MagicMock, patch

from src.database.preflight.RDSPreflight import RDSPreflight
from src.database.RDSConnector import RDSConnector


class RDSPreflightTestCase(unittest.TestCase):
    def setUp(self):
        self.preflight = RDSPreflight()

    @patch('src.database.RDSConnector.sessionmaker')
    @patch('src.database.RDSConnector.create_engine')
    def test_write_crawls(self, create_engine, sessionmaker):
        session = MagicMock()
        session.execute = MagicMock()
        session.commit = MagicMock()
        sessionmaker.return_value = session

        connector = RDSConnector()
        connector.connect()
        crawls = ['crawl1', 'crawl2']
        self.preflight.write_crawls(connector, crawls)

        connector.session.execute.assert_called_once()
        stmt = connector.session.execute.call_args[0][0]
        values = list(map(lambda x: x['path'], stmt.parameters))
        assert values == crawls, f'values is {values}'

        assert connector.session.commit.called
