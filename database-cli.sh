#!/bin/bash
source .env
docker run --rm --network=$DATABASE_NETWORK -it mongo:4.2 mongo --host $DATABASE_URL -u $DATABASE_USERNAME -p $DATABASE_PASSWORD --authenticationDatabase admin $DATABASE_NAME
