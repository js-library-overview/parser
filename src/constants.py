LOG_PATH = 'logs/'
LOG_CONF = 'logging.conf'
BASE_URL = 'https://commoncrawl.s3.amazonaws.com/'
COLL_INFO_URL = 'https://index.commoncrawl.org/collinfo.json'
