import os
from logging import config, getLogger, Formatter
from aws_logging_handlers.S3 import S3Handler

from dotenv import load_dotenv

from constants import LOG_CONF, LOG_PATH
from database.DatabaseConnector import DatabaseConnector
from database.MongoConnector import MongoConnector
from database.RDSConnector import RDSConnector
from database.preflight.Preflight import Preflight
from database.preflight.MongoPreflight import MongoPreflight
from database.preflight.RDSPreflight import RDSPreflight
from fetcher.fetcher import Fetcher
from fetcher.HttpFetcher import HttpFetcher
from fetcher.S3Fetcher import S3Fetcher
from log.RawKinesisHandler import RawKinesisHandler


class App:
    db_connector: DatabaseConnector = None
    preflight: Preflight = None
    fetcher: Fetcher = None

    def __init__(self):
        load_dotenv()
        if not os.path.exists(LOG_PATH):
            os.makedirs(LOG_PATH)
        config.fileConfig(LOG_CONF)

        local_dev = os.getenv('LOCAL_DEV', 'false').lower()
        if local_dev not in ['true', '1']:
            print('not local')
            # self.addS3Logger()
            # self.addKinesisLogger()

        fetch_type = os.getenv('FETCH_TYPE')
        if fetch_type == 'HTTP':
            self.fetcher = HttpFetcher()
        elif fetch_type == 'S3':
            self.fetcher = S3Fetcher()
        else:
            self.fetcher = HttpFetcher()

    def addS3Logger(self):
        bucket = os.getenv('BUCKET')
        log_name = 'timing'
        db_type = os.getenv('DB_TYPE').lower()
        logger = getLogger(log_name)
        handler = S3Handler(f'{db_type}/{log_name}/log', bucket, encryption_options={})
        formatter = logger.handlers[0].formatter
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    def addKinesisLogger(self):
        log_name = 'timing'
        stream_name = os.getenv('KINESIS_STREAM')
        db_type = os.getenv('DB_TYPE').lower()
        logger = getLogger(log_name)
        handler = RawKinesisHandler(stream_name)
        formatter = Formatter(f'%(levelname)s;{db_type};%(asctime)s;%(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    def connect(self) -> None:
        load_dotenv()
        db_type = os.getenv('DB_TYPE')
        if db_type == 'RDS':
            self.preflight = RDSPreflight()
            self.db_connector = RDSConnector()
        elif db_type == 'MONGO':
            self.preflight = MongoPreflight()
            self.db_connector = MongoConnector()
        else:
            self.preflight = MongoPreflight()
            self.db_connector = MongoConnector()

        self.db_connector.connect()

    def run(self) -> None:
        raise NotImplementedError('run needs to be customized')
