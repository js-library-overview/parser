import logging
from typing import Dict, List, Union

import warcio
from urllib3.exceptions import ProtocolError

from app import App
from tasks.LibraryTask import LibraryTask
from tasks.Task import Task
from util import timethis


class Parser(App):
    logger = logging.getLogger('parser')
    tasks: List[Dict[str, Union[str, Task]]] = []

    def add_task(self, name: str, task: Task) -> None:
        self.tasks.append({"name": name, "task": task})

    def run(self) -> None:
        self.logger.info("preflight started")
        self.preflight.run(self.db_connector, self.fetcher)
        self.logger.info("preflight finished")

        if len(self.tasks) == 0:
            self.logger.info("no tasks found")
            return

        self.logger.info('running tasks %s' % ([task['name'] for task in self.tasks]))
        while(True):
            try:
                self.run_tasks()
                break  # stop after one warc
            except ProtocolError as pe:
                self.logger.error(pe)
                self.logger.info("Aborted tasks due to network error")
                self.aborted = True

    @timethis
    def run_tasks(self) -> None:
        warc = None
        for task in self.tasks:
            if warc is None or self.db_connector.is_started_or_completed(task.name, warc):
                self.logger.info("getting new unprocessed warc")
                warc = self.db_connector.get_unprocessed_warc(task['name'])

            self.db_connector.mark_warc(warc, task['name'], 'started')

            self.logger.info("running task %s on warc %s" % (task['name'], warc['path']))

            status_code, body = self.fetcher.get_warc_stream(warc['path'])
            if status_code != 200:
                self.logger.warning("warc %s responded with code %d. Skipping." % (warc['path'], status_code))
                continue

            for record in warcio.ArchiveIterator(body):
                if record.rec_type == 'response':
                    rec_headers = record.rec_headers
                    http_headers = record.http_headers
                    data = record.content_stream().read()
                    task['task'].run(self.db_connector, rec_headers, http_headers, data, warc)

            task['task'].postprocessing(self.db_connector)
            task['task'].clean()

            self.db_connector.mark_warc(warc, task['name'], 'completed')
            self.logger.info('finished task')


if __name__ == "__main__":
    parser = Parser()
    parser.add_task("libraries", LibraryTask)
    parser.connect()
    parser.run()
