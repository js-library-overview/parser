import zlib
import boto3
from botocore import UNSIGNED
from botocore.client import Config

from .fetcher import Fetcher


class S3Fetcher(Fetcher):
    bucket = 'commoncrawl'
    s3client = boto3.client('s3', config=Config(signature_version=UNSIGNED))

    def get_warc_stream(self, path: str):
        obj = self.s3client.get_object(Bucket=self.bucket, Key=path)
        body = obj['Body']
        return [200, body]

    def get_coll_info(self):
        prefix = 'cc-index/collections/'
        response = self.s3client.list_objects_v2(
            Bucket=self.bucket,
            Prefix=prefix,
            Delimiter='/')
        indices = []
        for folder in response['CommonPrefixes']:
            id = folder.replace(prefix, '').rstrip('/')
            indices.append({'id': id})
        return [200, indices]

    def get_warc_paths(self, crawl_path: str):
        prefix = 'crawl-data/'
        file = '/warc.paths.gz'
        content = None
        response = self.s3client.get_object(Bucket=self.bucket, Key=f'{prefix}{crawl_path}{file}')
        content = zlib.decompress(response['Body'].read(), 32 + zlib.MAX_WBITS).decode('utf-8')
        return [200, content]
