import requests
import json
import zlib

from .fetcher import Fetcher
from constants import BASE_URL, COLL_INFO_URL


class HttpFetcher(Fetcher):
    def get_warc_stream(self, path: str):
        response = requests.get(BASE_URL + path, stream=True)
        return [response.status_code, response.raw]

    def get_coll_info(self):
        response = requests.get(COLL_INFO_URL)
        indices = None
        if response.status_code == 200:
            indices = json.loads(response.content)
        return [response.status_code, indices]

    def get_warc_paths(self, crawl_path: str):
        append = 'crawl-data/'
        file = '/warc.paths.gz'
        content = None
        response = requests.get(BASE_URL + append + crawl_path + file)

        if response.status_code == 200:
            content = zlib.decompress(response.content, 32 + zlib.MAX_WBITS).decode('utf-8')
        return [response.status_code, content]
