class Fetcher():
    def get_warc_stream(self, path: str):
        raise NotImplementedError('needs to be customized')

    def get_coll_info(self):
        raise NotImplementedError('needs to be customized')

    def get_warc_paths(self, crawl_path: str):
        raise NotImplementedError('needs to be customized')
