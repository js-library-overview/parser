from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey, DateTime
from .base import Base


class Warc_Tasks(Base):
    __tablename__ = "warc_tasks"

    warc_id = Column(Integer, ForeignKey('warc.id'), primary_key=True)
    warc = relationship("Warc")
    task_id = Column(Integer, ForeignKey('task.id'), primary_key=True)
    task = relationship("Task")
    started = Column(DateTime)
    completed = Column(DateTime)
