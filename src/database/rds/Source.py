from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Sequence

from .base import Base


class Source(Base):
    __tablename__ = "source"

    warc_relation = Table('source_warcs', Base.metadata,
                          Column('source_id', ForeignKey('source.id'), primary_key=True),
                          Column('warc_id', Integer, ForeignKey('warc.id'), primary_key=True)
                          )

    src = Column(String(10000), primary_key=True)
    url_id = Column(Integer, ForeignKey('url.id'), primary_key=True)
    id_seq = Sequence('seq_source_id', metadata=Base.metadata)
    id = Column(Integer, id_seq, server_default=id_seq.next_value(), unique=True)
    url = relationship("Url")
    warcs = relationship("Warc", secondary=warc_relation)

    def __repr__(self):
        return f"<Source(src='{self.src}', url='{self.url.path}', warcs='{self.warcs}')>"
