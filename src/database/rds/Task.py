from sqlalchemy import Column, String, Integer, Sequence

from .base import Base


class Task(Base):
    __tablename__ = "task"

    name = Column(String(50), primary_key=True)
    id_seq = Sequence('seq_task_id', metadata=Base.metadata)
    id = Column(Integer, id_seq, server_default=id_seq.next_value(), unique=True)

    def __repr__(self):
        return f"<Task(name='{self.name}')>"
