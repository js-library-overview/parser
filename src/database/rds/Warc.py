from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Sequence

from .base import Base


class Warc(Base):
    __tablename__ = "warc"

    path = Column(String(200), primary_key=True)
    id_seq = Sequence('seq_warc_id', metadata=Base.metadata)
    id = Column(Integer, id_seq, server_default=id_seq.next_value(), unique=True)
    timestamp = Column(DateTime)
    crawl_id = Column(Integer, ForeignKey('crawl.id'))
    crawl = relationship("Crawl", back_populates="warcs")

    def __repr__(self):
        return f"<Warc(path='{self.path}')>"
