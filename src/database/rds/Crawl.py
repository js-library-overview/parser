from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Sequence

from .base import Base


class Crawl(Base):
    __tablename__ = "crawl"

    path = Column(String(200), primary_key=True)
    id_seq = Sequence('seq_crawl_id', metadata=Base.metadata)
    id = Column(Integer, id_seq, server_default=id_seq.next_value(), unique=True)
    warc_count = Column(Integer, nullable=False, default=0)
    warcs = relationship("Warc", back_populates="crawl")

    def __repr__(self):
        return f"<Crawl(path='{self.path}')>"
