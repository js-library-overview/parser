from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Sequence

from .base import Base


class Url(Base):
    __tablename__ = "url"

    warc_relation = Table('url_warcs', Base.metadata,
                          Column('url_id', Integer, ForeignKey('url.id'), primary_key=True),
                          Column('warc_id', Integer, ForeignKey('warc.id'), primary_key=True)
                          )

    path = Column(String(10000), primary_key=True)
    id_seq = Sequence('seq_url_id', metadata=Base.metadata)
    id = Column(Integer, id_seq, server_default=id_seq.next_value(), unique=True)
    error = Column(String(1000))
    warcs = relationship("Warc", secondary=warc_relation)

    def __repr__(self):
        return f"<Url(path='{self.path}', warcs='{self.warcs}')>"
