import datetime
import logging
import re
from typing import Any, Dict, List, Optional

from constants import COLL_INFO_URL
from parser_types import Crawl, Warc
from database import DatabaseConnector
from database.static import static_crawl_paths
from fetcher.fetcher import Fetcher


class Preflight:
    logger = logging.getLogger('parser.preflight')

    def run(self, db_connector: DatabaseConnector, fetcher: Fetcher) -> None:
        raise NotImplementedError('needs to be customized')

    def create_collections_with_index(self, db_connector: DatabaseConnector) -> None:
        raise NotImplementedError('needs to be customized')

    def get_crawls(self, db_connector: DatabaseConnector, fetcher: Fetcher) -> None:
        self.logger.info('getting crawls')
        crawls = self.fetch_crawl_paths(fetcher)
        self.write_crawls(db_connector, crawls)

    def fetch_crawl_paths(self, fetcher: Fetcher) -> List[str]:
        return static_crawl_paths()

        # skipping for deterministic statistics
        status_code, indices = fetcher.get_coll_info()
        crawls: List[str] = []
        if status_code == 200:
            for index in indices:
                crawls.append(index['id'])
        else:
            self.logger.error('%s: %s for %s' % (status_code, indices, COLL_INFO_URL))
        return crawls

    def parse_warcs(self, crawl: Dict[str, Optional[Any]], fetcher: Fetcher) -> List[Warc]:
        if 'warcs' not in crawl:
            self.logger.info('getting warcs for crawl %s' % crawl['path'])

            status_code, content = fetcher.get_warc_paths(crawl['path'])

            if status_code == 200:
                regex = re.compile(r'segments/(.+)/warc/CC-MAIN-((\d{8}).+).warc.gz')
                warcs = [
                    {
                        'crawl': crawl['_id'],
                        'path': path,
                        'timestamp': datetime.datetime.strptime(regex.search(path).group(3), '%Y%m%d')
                    }
                    for path in content.split('\n') if path]
                return warcs
            else:
                self.logger.error('%d: %s for crawl %s'
                                  % (status_code, content, crawl['path']))
        return []

    def write_warcs(self, db_connector: DatabaseConnector, crawl: Crawl, warcs: List[Warc]) -> None:
        raise NotImplementedError('needs to be customized')

    def write_crawls(self, db_connector: DatabaseConnector, crawls: List[str]) -> None:
        raise NotImplementedError('needs to be customized')

    def get_warcs(self, db_connector: DatabaseConnector, fetcher: Fetcher) -> None:
        raise NotImplementedError('needs to be customized')

    def prepare_warc_queue(self, db_connector: DatabaseConnector) -> None:
        raise NotImplementedError('needs to be customized')
