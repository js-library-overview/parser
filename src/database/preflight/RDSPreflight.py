from typing import List
from sqlalchemy.dialects.postgresql import insert

from parser_types import Warc as WarcType
from database import RDSConnector
from .Preflight import Preflight
from database.rds.Crawl import Crawl
from database.rds.Warc import Warc
from database.rds.Task import Task
from database.rds.Warc_Tasks import Warc_Tasks
from database.rds.base import Base
from util import timethis
from database.static import get_queue_warcs, get_queue_tasks
from fetcher.fetcher import Fetcher


class RDSPreflight(Preflight):
    def run(self, db_connector: RDSConnector, fetcher: Fetcher) -> None:
        self.create_collections_with_index(db_connector)
        self.get_crawls(db_connector, fetcher)
        self.get_warcs(db_connector, fetcher)
        self.persist_tasks(db_connector)
        self.prepare_warc_queue(db_connector)

    def create_collections_with_index(self, db_connector: RDSConnector) -> None:
        Base.metadata.create_all(db_connector.db)

    @timethis
    def write_warcs(self, db_connector: RDSConnector, crawl: Crawl, warcs: List[WarcType]) -> None:
        session = db_connector.session
        for warc in warcs:
            warc['crawl_id'] = warc['crawl']
            del warc['crawl']
        crawl.warc_count = len(warcs)
        stmt = insert(Warc).values(warcs)
        stmt = stmt.on_conflict_do_nothing(index_elements=['path'])
        session.execute(stmt)
        session.commit()

    @timethis
    def write_crawls(self, db_connector: RDSConnector, crawls: List[str]) -> None:
        session = db_connector.session
        db_values = []
        for crawl in crawls:
            db_values.append({'path': crawl})
        stmt = insert(Crawl).values(db_values)
        stmt = stmt.on_conflict_do_nothing(index_elements=['path'])
        session.execute(stmt)
        session.commit()

    @timethis
    def get_warcs(self, db_connector: RDSConnector, fetcher: Fetcher) -> None:
        session = db_connector.session
        crawls: List[Crawl] = session.query(Crawl)
        for crawl in crawls:
            crawl_dict = {'_id': crawl.id, 'path': crawl.path}
            if crawl.warc_count > 0:
                crawl_dict['warcs'] = crawl.warcs
            warcs = self.parse_warcs(crawl_dict, fetcher)
            if len(warcs) > 0:
                self.write_warcs(db_connector, crawl, warcs)

    def prepare_warc_queue(self, db_connector: RDSConnector) -> None:
        queue_warcs = get_queue_warcs()
        queue_tasks = get_queue_tasks()
        session = db_connector.session
        warcs = session.query(Warc).filter(Warc.path.in_(queue_warcs)).all()
        tasks = session.query(Task).filter(Task.name.in_(queue_tasks)).all()
        for warc in warcs:
            for task in tasks:
                session.merge(Warc_Tasks(warc_id=warc.id, task_id=task.id))
        session.commit()

    @timethis
    def persist_tasks(self, db_connector: RDSConnector) -> None:
        session = db_connector.session
        tasks = get_queue_tasks()
        db_values = []
        for task in tasks:
            db_values.append({'name': task})

        stmt = insert(Task).values(db_values)
        stmt = stmt.on_conflict_do_nothing(index_elements=['name'])
        session.execute(stmt)
        session.commit()
