from typing import List

import pymongo
from pymongo import UpdateOne
from pymongo.database import Database
from pymongo.errors import BulkWriteError

from parser_types import Crawl, Warc
from database import MongoConnector
from .Preflight import Preflight
from util import timethis
from fetcher.fetcher import Fetcher


class MongoPreflight(Preflight):
    def run(self, db_connector: MongoConnector, fetcher: Fetcher) -> None:
        self.create_collections_with_index(db_connector)
        self.get_crawls(db_connector, fetcher)
        self.get_warcs(db_connector, fetcher)

    def create_collections_with_index(self, db_connector: MongoConnector) -> None:
        db: Database = db_connector.db
        db.crawls.create_index('path', unique=True)
        db.warcs.create_index('path', unique=True)
        db.warcs.create_index('crawl')
        db.warcs.create_index('started.task')
        db.warcs.create_index('completed.task')
        db.sources.create_index([('src', pymongo.ASCENDING), ('url', pymongo.ASCENDING)], unique=True)
        db.sources.create_index('library.name')
        db.sources.create_index([('library.name', pymongo.ASCENDING), ('library.version', pymongo.ASCENDING)])
        db.urls.create_index('path', unique=True)

    @timethis
    def write_warcs(self, db_connector: MongoConnector, crawl: Crawl, warcs: List[Warc]) -> None:
        db: Database = db_connector.db
        try:
            result = db.warcs.insert_many(warcs)
            db.crawls.update_one(crawl, {'$set': {'warcs': result.inserted_ids}})
        except BulkWriteError as bwe:
            self.logger.error(bwe.details)

    @timethis
    def write_crawls(self, db_connector: MongoConnector, crawls: List[str]) -> None:
        db: Database = db_connector.db
        requests = []
        for crawl in crawls:
            entry = {'path': crawl}
            requests.append(UpdateOne(entry, {'$set': entry}, upsert=True))
        if len(requests) != 0:
            db.crawls.bulk_write(requests)

    @timethis
    def get_warcs(self, db_connector: MongoConnector, fetcher: Fetcher) -> None:
        db: Database = db_connector.db
        crawls: List[Crawl] = db.crawls.find()
        for crawl in crawls:
            warcs = self.parse_warcs(crawl, fetcher)
            if len(warcs) > 0:
                self.write_warcs(db_connector, crawl, warcs)

    def prepare_warc_queue(self, db_connector: MongoConnector) -> None:
        None
