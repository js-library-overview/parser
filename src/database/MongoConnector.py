import datetime
from typing import List
from pymongo import MongoClient
from pymongo.database import Database
from pymongo.errors import DuplicateKeyError, BulkWriteError
from pymongo import ReturnDocument, UpdateOne
from lxml.etree import ParserError

from database.DatabaseConnector import DatabaseConnector
from parser_types import Warc
from tasks import LibraryTask
from util import timethis
from database.static import get_queue_warcs


class MongoConnector(DatabaseConnector):
    client: MongoClient = None
    db: Database = None
    url: str
    username: str
    password: str
    db_name: str

    def connect(self):
        self.client = MongoClient(
            self.url,
            username=self.username,
            password=self.password)
        self.db = self.client[self.db_name]

    def is_started_or_completed(self, task: str, warc: Warc) -> bool:
        try:
            return super().is_started_or_completed(task, warc)
        except NotImplementedError:
            warc = self.db.warcs.find_one({
                '_id': warc['_id'],
                '$or': {
                    'started.task': {'$in': [task['name']]},
                    'completed.task': {'$in': [task['name']]}
                }
            })
            return (warc is not None)

    def get_unprocessed_warc(self, task: str) -> Warc:
        warc = self.db.warcs.find_one({'path': {'$in': get_queue_warcs()}, 'started.task': {'$nin': [task]}})
        return warc

        # skipping for deterministic statistics
        result = self.db.warcs.aggregate([
            {'$unwind': {'path': '$started', 'preserveNullAndEmptyArrays': True}},
            {'$project': {
                'crawl': 1,
                'started': '$started.task'
            }},
            {'$group': {
                '_id': '$crawl',
                'processed': {'$sum': {'$cond': [{'$eq': [task, '$started']}, 1, 0]}}
            }},
            {'$sort': {'processed': 1}},
            {'$limit': 1},
            {'$project': {
                '_id': 0,
                'crawl': '$_id',
                'count': 1,
            }}
        ])
        crawl_id = list(result)[0]['crawl']
        warc = self.db.warcs.find_one({'crawl': crawl_id, 'started.task': {'$nin': [task]}})

        return warc

    @timethis
    def mark_warc(self, warc: Warc, task: str, status: str) -> None:
        self.db.warcs.update_one(
            {'_id': warc['_id']},
            {'$push': {status: {'task': task, 'timestamp': datetime.datetime.utcnow()}}})

    @timethis
    def write_url(self, url: str, warc: Warc) -> str:
        warc_id = warc['_id']
        try:
            document = self.db.urls.find_one_and_update(
                {'path': url},
                {
                    '$set': {'path': url},
                    '$addToSet': {'warcs': warc_id}},
                upsert=True,
                return_document=ReturnDocument.AFTER)
            return document['_id']
        except DuplicateKeyError:
            return self.write_url(self.db, url, warc)

    def write_url_error(self, url_id, error: ParserError) -> None:
        self.db.urls.update_one(
            {'_id': url_id},
            {
                '$set': {'error': str(error)}
            }
        )

    @timethis
    def write_sources(self, sources) -> None:
        source_requests = MongoConnector.create_requests_for_sources(sources)
        try:
            self.db.sources.bulk_write(source_requests, ordered=False)
        except BulkWriteError as bwe:
            LibraryTask.logger.error(bwe.details)

    @staticmethod
    def create_requests_for_sources(sources) -> List[UpdateOne]:
        source_requests: List[UpdateOne] = []
        for source in sources:
            src = source['src']
            url_id = source['url_id']
            library = source['library']
            warc_id = source['warc_id']
            request = UpdateOne(
                {'src': src, 'url': url_id},
                {
                    '$set': {
                        'src': src,
                        'url': url_id,
                        'library': library,
                    },
                    '$addToSet': {
                        'warcs': warc_id
                    }
                },
                upsert=True
            )
            source_requests.append(request)
        return source_requests
