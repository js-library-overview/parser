import os
from lxml.etree import ParserError
from dotenv import load_dotenv

from parser_types import Warc


class DatabaseConnector:
    url: str
    username: str
    password: str
    db_name: str

    def __init__(self):
        load_dotenv()
        self.url = os.getenv("DATABASE_URL")
        self.username = os.getenv("DATABASE_USERNAME")
        self.password = os.getenv("DATABASE_PASSWORD")
        self.db_name = os.getenv("DATABASE_NAME")

    def connect(self):
        raise NotImplementedError('needs to be customized')

    def is_started_or_completed(self, task: str, warc: Warc) -> bool:
        if 'started' in warc and any(started['task'] == task for started in warc['started']):
            return True
        if 'completed' in warc and any(completed['task'] == task for completed in warc['completed']):
            return True
        raise NotImplementedError('needs to be customized')

    def get_unprocessed_warc(self, task: str) -> Warc:
        raise NotImplementedError('needs to be customized')

    def mark_warc(self, warc: Warc, task: str, status: str) -> None:
        raise NotImplementedError('needs to be customized')

    def write_url(self, url: str, warc: Warc) -> str:
        raise NotImplementedError('needs to be customized')

    def write_url_error(self, url_id, error: ParserError) -> None:
        raise NotImplementedError('needs to be customized')

    def write_sources(self, sources) -> None:
        raise NotImplementedError('needs to be customized')
