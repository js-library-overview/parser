from typing import List


def static_crawl_paths() -> List[str]:
    return ['CC-MAIN-2020-50']


def get_queue_warcs() -> List[str]:
    prefix = 'crawl-data/CC-MAIN-2020-50/segments/1606141163411.0/warc/CC-MAIN-20201123153826-20201123183826-'
    suffix = '.warc.gz'
    warcs = []
    for i in range(100):
        count = str(i).zfill(5)
        warcs.append(f'{prefix}{count}{suffix}')
    return warcs


def get_queue_tasks() -> List[str]:
    return ['libraries']
