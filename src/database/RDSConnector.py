import datetime
from lxml.etree import ParserError
from typing import List, Any
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert

from parser_types import Warc
from database.DatabaseConnector import DatabaseConnector
from database.rds.Url import Url
from database.rds.Source import Source
from database.rds.Warc_Tasks import Warc_Tasks
from database.rds.Task import Task
from util import timethis


class RDSConnector(DatabaseConnector):
    db: Any
    session: Any

    def connect(self):
        Session = sessionmaker()
        self.db = create_engine(f"postgresql://{self.username}:{self.password}@{self.url}/{self.db_name}",
                                echo=False)
        Session.configure(bind=self.db)
        self.session = Session()

    def is_started_or_completed(self, task: str, warc: Warc) -> bool:
        query = self.session.query(Warc_Tasks)\
                    .join(Task)\
                    .filter(Warc_Tasks.warc_id == warc.id,
                            Task.name == task['name'],
                            Warc_Tasks.started == None)  # noqa: E711
        return query.count() == 0

    @timethis
    def get_unprocessed_warc(self, task: str) -> Warc:
        query = self.session.query(Warc_Tasks).filter(Warc_Tasks.started == None)  # noqa: E711
        warc = query.first().warc
        return {'path': warc.path, '_id': warc.id, 'rds_warc': warc}

    @timethis
    def mark_warc(self, warc: Warc, task: str, status: str) -> None:
        query = self.session.query(Warc_Tasks)\
                    .filter(Warc_Tasks.warc_id == warc['_id'], Warc_Tasks.task.has(name=task))
        now = datetime.datetime.utcnow()
        entry = query.first()
        setattr(entry, status, now)
        self.session.commit()

    @timethis
    def write_url(self, path: str, warc: Warc) -> str:
        url = Url(path=path)
        url.warcs.append(warc['rds_warc'])
        self.session.add(url)
        self.session.commit()
        self.session.refresh(url)
        return url.id

    def write_url_error(self, url_id, error: ParserError) -> None:
        url = self.session.query(Url).filter(Url.id == url_id).one()
        url.error = str(error)
        self.session.commit()

    @timethis(log_args=False)
    def write_sources(self, sources) -> None:
        source_requests = RDSConnector.create_requests_for_sources(sources)
        stmt = insert(Source).values(source_requests)
        # TODO: fix updating
        stmt = stmt.on_conflict_do_nothing(index_elements=['src', 'url_id'])
        self.session.execute(stmt)
        self.session.commit()

    @staticmethod
    def create_requests_for_sources(sources) -> List[Source]:
        source_requests: List[Source] = []
        for source in sources:
            src = source['src']
            url_id = source['url_id']
            # warc = source['warc']['rds_warc']
            # TODO: properly add warcs
            request = {'src': src, 'url_id': url_id}
            # request = Source(src=src, url_id=url_id)
            # request.warcs.append(warc)
            source_requests.append(request)
        return source_requests
