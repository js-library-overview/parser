import logging
import re
from typing import Dict, List, Optional, Any

from database import DatabaseConnector

from lxml.etree import ParserError, HTMLParser
from lxml.html import fromstring

from tasks.Task import Task
from parser_types import Warc
from util import timethis


class LibraryTask(Task):

    logger = logging.getLogger('task.library')
    charset_pattern = re.compile(r'charset=["\']?([^;\s]+)["\']?')
    name_pattern = re.compile(r'/?([^/?]*)(?:/?\?.*)?$')
    version_pattern = re.compile(r'')
    javascript_pattern = re.compile(r'(text|application)/javascript', re.I)

    source_requests: List[Any] = []

    @staticmethod
    def get_script_sources(data, encoding) -> List[str]:
        sources = []
        if encoding != '':
            page = fromstring(data, parser=HTMLParser(encoding=encoding))
        else:
            page = fromstring(data)
        scripts = page.xpath('//*/script')
        for script in scripts:
            script_type = script.get('type')
            src = script.get('src')
            if src is not None:
                if script_type is not None and LibraryTask.javascript_pattern.search(script_type) is None:
                    continue
                sources.append(src)

        return sources

    @staticmethod
    def get_encoding(content_type, data) -> str:
        # no specific fallback encoding is set, as lxml will try to deduce the encoding if none is defined
        fallback_encoding = ''
        charset = LibraryTask.extract(content_type, LibraryTask.charset_pattern)
        if charset is not None and charset != '':
            return charset
        return fallback_encoding

    @staticmethod
    def create_requests_for_sources(sources: List[str], url_id, warc: Warc) -> None:
        for src in sources:
            library = LibraryTask.get_library(src)
            source = {
                'src': src,
                'url_id': url_id,
                'library': library,
                'warc_id': warc['_id'],
                'warc': warc,
            }
            LibraryTask.source_requests.append(source)

    @staticmethod
    def get_library(src: str) -> Dict[str, Optional[str]]:
        name = LibraryTask.extract(src, LibraryTask.name_pattern)
        # version = LibraryTask.extract(src, LibraryTask.version_pattern)
        # TODO: elaborate how the version may look like
        version = 'unknown'
        return {'name': name, 'version': version}

    @staticmethod
    def extract(src, pattern, group=1) -> str:
        match = pattern.search(src)
        result = match.group(group) if match is not None else None
        return result

    @staticmethod
    def postprocessing(db_connector: DatabaseConnector) -> None:
        if(len(LibraryTask.source_requests) != 0):
            db_connector.write_sources(LibraryTask.source_requests)

    @staticmethod
    def clean() -> None:
        LibraryTask.source_requests = []

    @staticmethod
    @timethis(log_args=False)
    def run(db_connector: DatabaseConnector, rec_headers: Dict[str, Any],
            http_headers: Dict[str, Any], data, warc: Warc) -> None:
        url = rec_headers.get_header('WARC-Target-URI')
        content_type = http_headers.get_header('content-type', None)
        if content_type is None or 'html' not in content_type:
            return
        encoding = LibraryTask.get_encoding(content_type, data)
        parser_error = None
        try:
            sources = LibraryTask.get_script_sources(data, encoding)
        except ParserError as pe:
            parser_error = pe
        except LookupError as le:
            parser_error = le
        except Exception as e:
            parser_error = e

        url_id = db_connector.write_url(url, warc)

        if parser_error is not None:
            db_connector.write_url_error(url_id, parser_error)
            return
        LibraryTask.create_requests_for_sources(sources, url_id, warc)
