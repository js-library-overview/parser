from typing import Dict, Any
from database import DatabaseConnector
from parser_types import Warc


class Task:
    def postprocessing(db_connector: DatabaseConnector) -> None:
        raise NotImplementedError('run needs to be customized')

    def clean() -> None:
        raise NotImplementedError('run needs to be customized')

    def run(db_connector: DatabaseConnector, rec_headers: Dict[str, Any],
            http_headers: Dict[str, Any], data, warc: Warc) -> None:
        raise NotImplementedError('run needs to be customized')
