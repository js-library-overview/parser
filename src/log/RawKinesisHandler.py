from logging import StreamHandler
import boto3


class RawKinesisHandler(StreamHandler):
    def __init__(self, stream_name: str, partition_key: str = 'us-east-1'):
        StreamHandler.__init__(self)
        self.stream_name = stream_name
        self.partition_key = partition_key

        self.client = boto3.client('kinesis', 'us-east-1')

    def emit(self, record):
        msg = self.format(record)
        self.client.put_record(StreamName=self.stream_name, Data=str.encode(msg), PartitionKey=self.partition_key)
