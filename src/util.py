import inspect
import logging
import time
from functools import wraps, partial

from database.DatabaseConnector import DatabaseConnector
from database.preflight.Preflight import Preflight


def timethis(func=None, *, log_args=True, log_if_zero=True):

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            logger = logging.getLogger('timing')

            start = time.time()
            result = func(*args, **kwargs)
            end = time.time()
            duration = round(end - start, 0)

            if log_if_zero is False and duration == 0:
                return result

            defining_class = get_class_that_defined_method(func)

            params = []
            if log_args is True:
                for arg in args:
                    arg_str = stringify_arg(arg)
                    if arg_str is not None:
                        params.append(f'{arg_str}')
                for key, value in kwargs.items():
                    arg_str = stringify_arg(value)
                    if arg_str is not None:
                        params.append(f'{key}={arg_str}')
            logger.info(f'{defining_class};{func.__name__};({", ".join(params)});{duration:.0f}')
            return result
        return wrapper
    if func:
        return decorator(func)
    else:
        return decorator


def stringify_arg(arg):
    ignored_arg_types = (Preflight, DatabaseConnector)
    if (isinstance(arg, ignored_arg_types)):
        return None
    if (isinstance(arg, list)):
        if(len(arg) > 1):
            return f'...{arg[-1:]}'
    if(isinstance(arg, str)):
        if(len(arg) > 200):
            arg = arg[:200]
        return arg.replace("\n", " ")
    return f'{arg}'


# taken from http://stackoverflow.com/a/25959545/3903832
def get_class_that_defined_method(meth):
    if isinstance(meth, partial):
        return get_class_that_defined_method(meth.func)
    if inspect.ismethod(meth) or (inspect.isbuiltin(meth) and getattr(meth, '__self__', None) is not None and getattr(meth.__self__, '__class__', None)):  # noqa: E501
        for cls in inspect.getmro(meth.__self__.__class__):
            if meth.__name__ in cls.__dict__:
                return cls
        meth = getattr(meth, '__func__', meth)  # fallback to __qualname__ parsing
    if inspect.isfunction(meth):
        cls = getattr(inspect.getmodule(meth),
                      meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0],
                      None)
        if isinstance(cls, type):
            return cls
    return getattr(meth, '__objclass__', None)  # handle special descriptor objects
