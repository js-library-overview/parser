import logging

from app import App


class Aggregator(App):
    logger = logging.getLogger('aggregator')

    def aggregate_timeline(self, library: str, place: int) -> None:
        self.db.sources.aggregate([
            {'$match': {'library.name': library}},
            {'$lookup': {
                'from': 'warcs',
                'localField': 'warcs',
                'foreignField': '_id',
                'as': 'warcs',
            }},
            {'$unwind': {'path': '$warcs'}},
            {'$project': {
                '_id': 1,
                'timestamp': '$warcs.timestamp',
            }},
            {'$group': {
                '_id': '$timestamp',
                'count': {'$sum': 1}

            }},
            {'$project': {
                '_id': 1,
                'library': library,
                'count': 1
            }},
            {'$sort': {'_id': 1}},
            {'$group': {
                '_id': '$library',
                'timestamps': {'$push': {'timestamp': '$_id', 'count': '$count'}}
            }},
            {'$addFields': {
                'place': place
            }},
            {'$project': {
                '_id': '$place',
                'library': '$_id',
                'timestamps': 1
            }},
            {'$merge': {
                'into': 'librarytimelines',
                'on': '_id',
                'whenMatched': 'replace',
                'whenNotMatched': 'insert'
            }}
        ], allowDiskUse=True)

    def aggregate_complete(self) -> None:
        one_minute = 60 * 1000
        self.db.warcs.aggregate([
            {'$unwind': {'path': '$completed'}},
            {'$unwind': {'path': '$started'}},
            {'$project': {
                '_id': 1,
                'path': 1,
                'completed': 1,
                'started': 1,
                'duration': {'$subtract': ['$completed.timestamp', '$started.timestamp']},
            }},
            {'$sort': {'duration': 1}},
            {'$group': {
                '_id': '$completed.task',
                'first_completed': {'$min': '$completed.timestamp'},
                'last_completed': {'$max': '$completed.timestamp'},
                'first_started': {'$min': '$started.timestamp'},
                'last_started': {'$max': '$started.timestamp'},
                'avg_time': {'$avg': '$duration'},
                'min_time': {'$min': '$duration'},
                'max_time': {'$max': '$duration'},
                'deviation': {'$stdDevPop': "$duration"},
                'count': {'$sum': 1}
            }},
            {'$project': {
                '_id': 1,
                'first_completed': 1,
                'last_completed': 1,
                'first_started': 1,
                'last_started': 1,
                'avg_time': {'$divide': ['$avg_time', one_minute]},
                'min_time': {'$divide': ['$min_time', one_minute]},
                'max_time': {'$divide': ['$max_time', one_minute]},
                'deviation': {'$divide': ['$deviation', one_minute]},
                'count': 1,
            }},
            {'$out': 'completedwarcs'}
        ])

    def aggregate_top(self) -> None:
        self.db.sources.aggregate([
            {'$sort': {'library.name': 1}},
            {'$group': {
                '_id': '$library.name',
                'amount': {'$sum': 1}
            }},
            {'$sort': {'amount': -1}},
            {'$out': 'toplibs'}
        ], allowDiskUse=True)

    def aggregate_url_count(self) -> None:
        self.db.urls.aggregate([
            {'$lookup': {
                'from': 'warcs',
                'localField': 'warcs',
                'foreignField': '_id',
                'as': 'warcs',
            }},
            {'$unwind': {'path': '$warcs'}},
            {'$project': {
                '_id': 1,
                'timestamp': '$warcs.timestamp',
            }},
            {'$group': {
                '_id': '$timestamp',
                'count': {'$sum': 1}

            }},
            {'$sort': {'_id': 1}},
            {'$merge': {
                'into': 'urlcounttimelines',
                'on': '_id',
                'whenMatched': 'replace',
                'whenNotMatched': 'insert'
            }}
        ], allowDiskUse=True)

    def run(self) -> None:
        self.logger.info('started url count timeline')
        self.aggregate_url_count()
        self.logger.info('finished url count timeline')
        self.logger.info('started top library aggregation')
        self.aggregate_top()
        self.logger.info('finished top library aggregation')
        self.logger.info('started completion aggregation')
        self.aggregate_complete()
        self.logger.info('finished completion aggregation')
        self.logger.info('dropping timeline aggregations')
        libraries = self.db['toplibs'].find().limit(10)
        self.db['librarytimelines'].drop()
        place = 1
        for library in libraries:
            self.logger.info('started timline aggregation for %s' % library)
            self.aggregate_timeline(library['_id'], place)
            self.logger.info('finished timline aggregation for %s' % library)
            place += 1


if __name__ == '__main__':
    aggregator = Aggregator()
    aggregator.connect()
    aggregator.run()
