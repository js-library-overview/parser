from typing import Any, Dict, Optional

Crawl = Dict[str, Optional[Any]]
Warc = Dict[str, Optional[Any]]
Url = Dict[str, Optional[Any]]
Library = Dict[str, Optional[Any]]
